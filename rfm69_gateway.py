#!/usr/bin/env python2

import RFM69, ConfigParser, os, utils, datetime, time, logs, socket, threading, sys, pytz, ast, json
#import RPi.GPIO as GPIO
import CHIP_IO.GPIO as GPIO
import paho.mqtt.client as mqtt
from RFM69registers import *
import datetime
import time

logger = logs.setup_logger('rfm69_gateway')
logger.warning("Program Started...")
starttime = time.time()

### Get run mode
if len(sys.argv) > 1:
	runmode = sys.argv[1]
elif len(sys.argv) == 1:
	runmode = 'normal'
	
### This thread maintains envriomental variables and restarts program's threads in case they crash for any reason
class supervisor(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		while True:
			try:
				# Start MQTT thread if none running
				if "mqtt_thread" not in get_threads():
					thread = mqtt_connect(name = 'mqtt_thread')
					thread.daemon = True
					thread.start()
					logger.warning("Restarting mqtt_connect thread...")
				# Start read_xbee_frame threads if not already running, one per xbee modem configured
				for transceiver, attributes in transceiver_rfs.iteritems():
					if str(transceiver)+"_dispatcher_thread" not in get_threads():
						thread_name = str(transceiver)+"_dispatcher_thread"
						thread = dispatcher(config['mqtt_publish_interval'], thread_name)
						thread.daemon = True
						thread.start()
						logger.warning("Restarted dead dispatcher thread for xbee transceiver '%s' ..." % transceiver)
				time.sleep(1)
			except Exception:
				sys.excepthook(*sys.exc_info())
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print "Other error or exception occurred!"

class read_rfm69_frame(threading.Thread):
	def __init__(self, rfm69_transceiver_id, name = None):
		threading.Thread.__init__(self, name = name)
		self.id = rfm69_transceiver_id
		self.rfm69 = transceiver_rfs[self.id]['rfm69_object']
		self.modulation = transceiver_rfs[self.id]['modulation']
		self.bandwidth = transceiver_rfs[self.id]['bandwidth']
		self.latitude = config['gateway_latitude']
		self.longitude = config['gateway_longitude']
		self.altitude = config['gateway_altitude']
		self.mac = config['gateway_mac']
		self.fcnt = 0
		### Initiate containers for MQTT payload here
		self.payload = {'txInfo':{'frequency':915000000, 'dataRate':{'modulation':self.modulation, 'bandwidth':self.bandwidth}}}
		return
	def run(self):
		while True:
			try:
				### Frame processing starts here
				self.rfm69.receiveBegin()
				while not self.rfm69.receiveDone():
					time.sleep(.01)
				#rx_blink = blink(config['gateway_rxled'], 1)
				#rx_blink.start()
				# Store these two before they get cleaned up by the ACKRequested call
				senderid = self.rfm69.SENDERID
				rssi = self.rfm69.RSSI
				if self.rfm69.ACKRequested():
					self.rfm69.sendACK()
				#	tx_blink = blink(config['gateway_txled'], 1)
				#	tx_blink.start()
				# Process details from device
				devicename = 'lora'+str(senderid)
				if senderid < 10:
					senderid = '0'+str(format(senderid, 'x'))
				else:
					senderid = str(format(senderid, 'x'))
				deveui = self.mac[0:14]+senderid
				self.fcnt += 1
				# Process payload
				frame_payload = utils.parse_payload("".join([chr(letter) for letter in self.rfm69.DATA]))
				frame_payload = utils.decode(frame_payload)
				### For debugging porpuses we print all frames when running in debug mode
				if runmode == 'debug':
					print "##### ##### DEBUG %s ##### ##### #####" % self.name
					print "Received %s from %s with rssi %s" % (frame_payload, deveui, rssi)
					print "##### END #####"
				# Add RSSI and other rxinfo parameters
				rxinfo = {'rxInfo':[{'mac':str(self.mac),'latitude':float(self.latitude),'longitude':float(self.longitude),'altitude':int(self.altitude), 'rssi':rssi}]}
				self.payload.update({'object': frame_payload})
				self.payload.update(rxinfo)
				self.payload.update({'deviceName':devicename,'devEUI':deveui})
				self.payload.update({'fCnt':self.fcnt})
				publish_mqtt(self.payload)
			except Exception:
				sys.excepthook(*sys.exc_info())
				break
			except:
				logger.exception("Other error or exception occurred!")
				if runmode == 'debug':
					print "Other error or exception occurred!"
					
        
def Blink(gpio, times):
	for i in range (0, times):
		GPIO.output(gpio, True)
		time.sleep(0.5)
		GPIO.output(gpio, False)
		time.sleep(0.5)
        
class blink(threading.Thread):
	def __init__(self, led, times):
		threading.Thread.__init__(self)
		self.times = times
		self.led = led
		return
	def run(self):
		try:
			for i in range (0, self.times):
				GPIO.output(self.led, True)
				time.sleep(0.10)
				GPIO.output(self.led, False)
				time.sleep(0.10)
		except Exception:
			sys.excepthook(*sys.exc_info())

def gpio_config():
	GPIO.setwarnings(False)
	# Configure LED GPIO's
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(config['gateway_rxled'], GPIO.OUT)
	GPIO.setup(config['gateway_txled'], GPIO.OUT)

def config_load():
	global config
	global transceivers
	config = {}
	transceivers = {}
	config_file = ConfigParser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'gateway_id':utils.chop_comment(config_file.get('gateway', 'id')),
	'gateway_rxled':utils.chop_comment(config_file.get('gateway', 'rxled')),
	'gateway_txled':utils.chop_comment(config_file.get('gateway', 'txled')),
	'gateway_latitude':float(utils.chop_comment(config_file.get('gateway', 'latitude'))),
	'gateway_longitude':float(utils.chop_comment(config_file.get('gateway', 'longitude'))),
	'gateway_altitude':int(utils.chop_comment(config_file.get('gateway', 'altitude'))),
	'gateway_mac':utils.chop_comment(config_file.get('gateway', 'mac')),
	'transceiver_id':int(utils.chop_comment(config_file.get('transceiver', 'transceiver_id'))),
	'transceiver_networkid':int(utils.chop_comment(config_file.get('transceiver', 'networkid'))),
	'transceiver_freq':utils.chop_comment(config_file.get('transceiver', 'freq')),
	'transceiver_isRFM69HW':config_file.getboolean('transceiver', 'isRFM69HW'),
	'transceiver_modulation':utils.chop_comment(config_file.get('transceiver', 'modulation')),
	'transceiver_bandwidth':int(utils.chop_comment(config_file.get('transceiver', 'bandwidth'))),
	'transceiver_intpin':utils.chop_comment(config_file.get('transceiver', 'intpin')),
	'transceiver_rstpin':utils.chop_comment(config_file.get('transceiver', 'rstpin')),
	'transceiver_spibus':int(utils.chop_comment(config_file.get('transceiver', 'spibus'))),
	'transceiver_spidevice':int(utils.chop_comment(config_file.get('transceiver', 'spidevice'))),
	'mqtt_broker_hostname':utils.chop_comment(config_file.get('mqtt_broker', 'hostname')),
	'mqtt_broker_port':int(utils.chop_comment(config_file.get('mqtt_broker', 'port'))),
	'mqtt_broker_username':utils.chop_comment(config_file.get('mqtt_broker', 'username')),
	'mqtt_broker_pass':utils.chop_comment(config_file.get('mqtt_broker', 'password')),
	'mqtt_publish_topic':utils.chop_comment(config_file.get('mqtt_broker', 'publish_topic')),
	'mqtt_subscribe_topic':utils.chop_comment(config_file.get('mqtt_broker', 'subscribe_topic')),
	'timezone':config_file.get('environment', 'timezone'),
	})
	for section in config_file.sections():
		if section.startswith('transceiver'):
			for key, value in config_file.items(section):
				if key == 'transceiver_id':
					transceiver_id = value
					transceivers.update({transceiver_id:{'settings':{}}})
					for key, value in config_file.items(section):
						if key == 'transceiver_id':
							continue
						else:
							value = utils.chop_comment(value)
							transceivers[transceiver_id]['settings'].update({key:value})
 
def transceiver_load():
	global transceiver_rfs
	transceiver_rfs = {}
	for transceiver_id, attributes in transceivers.iteritems():
		rfm69 = RFM69.RFM69(RF69_915MHZ , int(transceiver_id), int(attributes['settings']['networkid']), bool(attributes['settings']['isrfm69hw']), attributes['settings']['intpin'], attributes['settings']['rstpin'], int(attributes['settings']['spibus']), int(attributes['settings']['spidevice']))
		logger.warning("Reading rfm69 temperature: '%s'." % rfm69.readTemperature(0)) 
		if attributes['settings']['isrfm69hw']:
			logger.warning("Turning rfm69hw high power on.")
			rfm69.setHighPower(True)
		logger.warning("Performing rfm69 rcCalibration.")
		rfm69.rcCalibration()
		logger.warning("Turning rfm69 encryption on.")
		rfm69.encrypt(attributes['settings']['encryptkey'])
		transceiver_rfs.update({transceiver_id:{'rfm69_object':rfm69,'modulation':attributes['settings']['modulation'], 'bandwidth':attributes['settings']['bandwidth']}})

### MQTT funcionts here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Succesfully connected to MQTT broker, starting subscriptions if any.")
		# Start Subscriptions here
		client.subscribe(config['mqtt_subscribe_topic'], 1)
		logger.warning("Subscribed to topic '%s'" % config['mqtt_subscribe_topic'])
	else:
		logger.warning("Connection to MQTT broker failed. rc='%s' " % s)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH '%s' completed." % mid)

def on_message(client, userdata, msg):
	if msg.topic == config['mqtt_subscribe_topic']:
		logger.info("Processing Publish '%s'." % msg.payload)
		# First convert payload to a dictionary


def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

def publish_mqtt(payload):
	from datetime import datetime
	### Add timestamp
	now = datetime.now(pytz.timezone(config['timezone'])).strftime('%Y-%m-%d %H:%M:%S')
	rxinfo = payload['rxInfo'][0]
	rxinfo.update({'time':now})
	del payload['rxInfo']
	payload.update({'rxInfo':[rxinfo]})
	# Pass the container to the MQTT thread for publishing
	mqttc.publish('%s/%s/rx' % (config['mqtt_publish_topic'], config['gateway_mac']), json.dumps(payload))
	if runmode == 'debug':
		print "# MQTT: '%s/%s %s'" % (config['mqtt_publish_topic'], config['gateway_mac'], payload)
		print "##### END #####"

### Classes / functions start here
class mqtt_connect(threading.Thread):
	def __init__(self, name = None):
		threading.Thread.__init__(self, name = name)
		return
	def run(self):
		global mqttc
		mqttc = mqtt.Client(config['gateway_id'])
		mqttc.username_pw_set(config['mqtt_broker_pass'], config['mqtt_broker_pass'])
		mqttc.connect(config['mqtt_broker_hostname'], config['mqtt_broker_port'], 80)
		mqttc.on_connect = on_connect
		#mqttc.on_message = on_message
		mqttc.on_publish = on_publish
		mqttc.on_disconnect = on_disconnect
		# Uncomment here to enable MQTT debug messages
		#mqttc.on_log = on_log
		while True:
			mqttc.loop()

def check_network():
	loop = 1
        while loop == 1:
		try:
 			s = socket.create_connection((socket.gethostbyname(config['mqtt_broker_hostname']), config['mqtt_broker_port']), 2)
			logger.warning("Network checks passed!, '%s' is reachable." % config['mqtt_broker_hostname'])
			loop = 0
		except Exception:
			logger.warning("Network checks failed!, stalling until MQTT Broker '%s' becomes reachable..." % config['mqtt_broker_hostname'])
			time.sleep(15)
			
if __name__ == "__main__":
	print "rfm69_gateway is spinning. Check /var/log/rfm69_gateway.log for more details..."
	# Load environment variables and config parameters from file config.cfg
	logger.warning("Loading configuration parameters from config file.")
	config_load()
	# Configure GPIO's
	gpio_config()
	logger.warning("Configuring board GPIO's.")
	# Perfrom network checks and wait there until we can reach the MQTT broker
	logger.warning("Connecting to MQTT Broker.")
	check_network()
	# Create xbee transceiver objects
	logger.warning("Starting tranceivers.")
	transceiver_load()
	try:
		# Start MQTT thread here
		thread = mqtt_connect(name = 'mqtt_thread')
		thread.daemon = True
		thread.start()
		logger.warning("Starting mqtt_connect thread...")
		for rfm69_transceiver_id, attributes in transceiver_rfs.iteritems():
			thread_name = str(rfm69_transceiver_id)+"_gatewayid_read_rfm69_frame_thread"
			thread = read_rfm69_frame(rfm69_transceiver_id, thread_name)
			thread.daemon = True
			thread.start()
			logger.warning("Starting a read_rfm69_frame thread for rfm69 transceiver '%s' ..." % rfm69_transceiver_id)
			time.sleep(1)
	except KeyboardInterrupt:
		logger.exception("Got exception on main handler:")
		if runmode == 'debug':
			print "Got exception on main handler:"
		mqttc.loop_stop()
		mqttc.disconnect()
		GPIO.cleanup()
		raise

	except:
		logger.exception("Other error or exception occurred!")
		if runmode == 'debug':
			print "Other error or exception occurred!"
		mqttc.loop_stop()
		mqttc.disconnect()
		GPIO.cleanup()
		for rfm69_transceiver_id, attributes in transceiver_rfs.iteritems():
			attributes['rfm69_object'].shutdown()

	while True:
		time.sleep(1)
		pass










