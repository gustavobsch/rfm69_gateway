#!/usr/bin/python
import logging, json
from Queue import PriorityQueue
logger = logging.getLogger('rfm69_gateway')
logger.info("Library utils.py started...")

def chop_comment(line):
	c_backslash = '\\'
	c_dquote = '"'
	c_comment = '#'
	space = ' '
	# a little state machine with two state varaibles:
	in_quote = False  # whether we are in a quoted string right now
	backslash_escape = False  # true if we just saw a backslash
	for i, ch in enumerate(line):
		if not in_quote and ch == c_comment:
			# not in a quote, saw a '#', it's a comment.  Chop it and return!
			return line[:i]
		elif not in_quote and ch == space:
			# not in a quote, saw a '#', it's a comment.  Chop it and return!
			return line[:i]
		elif backslash_escape:
			# we must have just seen a backslash; reset that flag and continue
			backslash_escape = False
		elif in_quote and ch == c_backslash:
			# we are in a quote and we see a backslash; escape next char
			backslash_escape = True
		elif ch == c_dquote:
			in_quote = not in_quote
	return line

def parse_payload(frame):
	try:
		#try to parse json frame payload
		frame = json.loads(frame.decode('utf-8'))
		return frame
	except ValueError:  # includes simplejson.decoder.JSONDecodeError
		logger.warning("A JSON payload decode error just happened!")

def sensor_map(sensor):
	sensor_map = {
		# vcc measurements 1 - 9
		'vcc': 						'1',
		'battery_per': 				'2',
		'battery_voltage':			'3',
		'battery_level':			'4',
		# weather sensors 10 - 19
		'temperature':				'10',
		'onboard_temp':				'11',
		'pressure':					'12',
		# humidty, rain 20-29
		'humidity':					'20',
		'rain_volt':				'21',
		# soil sensors 30 - 39
		# air sensors 40 - 49
		'dust':						'40',
		'co2':						'41',
		'gas':						'42',
		'smoke':					'43',
		# gps related 50 - 69
		'altitude':					'50',
		# ascelerometers 70 - 79
		# color - light sensors 80 -89
		'r':						'80',
		'g':						'81',
		'b':						'82',
		'c':						'83',
		'lux':						'84',
		'color_temp':				'85',
		# signal strength 90 - 99
		'rssi':						'90',
		'ec':						'91',
		'ea':						'92',
		# other stuff 100 - 130
		'external_water_tank':		'100',
		'exposed_temp_sensor':		'101',
		'bed_temperature':			'102',
                # presence 
                'pir':                                          '131'
	}
	for key, value in sensor_map.items():
		if sensor == value:
			return key
	return sensor

def decode(payload):
	for key, value in payload.iteritems():
		payload[sensor_map(key)] = payload.pop(key)
	return payload

